<!DOCTYPE html>
<html lang="<?php language_attributes(); ?>">
<head>
    <?php wp_head(); ?>

    <!-- GOOGLE ANALYTICS -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-149361525-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'UA-149361525-1');
    </script>


    <!-- BALISES META -->
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Alexandre Tourigny, Sébastian Macias & Léa Kelly">
    <meta name="description"
          content="Un restaurant classique servant des repas européens, de la fondue au fromage et de la raclette dans une ambiance festive ou les visiteurs peuvent apporter leur propre vin.">
    <meta name="keywords"

    <!-- TITRE -->
    <title><?php bloginfo('name') . ' - ' . wp_title(); ?></title>


    <!-- STYLESHEETS -->
    <link href="https://fonts.googleapis.com/css?family=Oranienbaum|Pacifico|Prata&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
          integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/swiper.min.css">
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/normalize.css">
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">


    <!-- SCRIPTS -->
    <script src="<?php bloginfo('template_url'); ?>/js/swiper.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/script.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/scriptEmporter.js"></script>

</head>
<body <?php body_class(); ?>>

<!--------------------------------------------------------------->
<!-- NAVIGATION -->
<header>
    <?php wp_nav_menu(array(
        'theme_location' => 'menu_principal',
        'container' => 'nav'
    )); ?>

    <?php wp_nav_menu(array(
        'theme_location' => 'menu_langues'
    )); ?>
</header>
<!--------------------------------------------------------------->