<?php
/* Template Name: Menu pour emporter */

get_header();
?>


    <section class="contenuEmporter">


        <h3>Pour emporter</h3>
        <hr>

        <h4><?php  the_field('acf_of_telephone', 'option'); ?></h4>


        <section class="sectionEmporter">

            <div class="choixEmporter">

                <a href="#" class="choixEmporterItem choixEntrees"  id="choixEntrees">
                    <p>Entrées</p>
                </a>

                <a href="#" class="choixEmporterItem choixPlatsPrincipaux" id="choixPlatsPrincipaux">
                    <p>Plats principaux</p>
                </a>

                <a href="#" class="choixEmporterItem choixACotes" id="choixACotes">
                    <p>Les à-côtés</p>
                </a>

                <a href="#" class="choixEmporterItem choixDesserts" id="choixDesserts">
                    <p>Desserts</p>
                </a>

            </div>

            <div class="repasEmporter repasEntrees" id="repasEntrees">
                <?php query_posts(array('post_type' => 'post_type_repas_empo')); ?>

                <?php if ( have_posts()) : ?>

                    <?php while (have_posts()) : the_post(); ?>

                        <?php if ( get_field('type_de_repas') == 'Entrée') : ?>

                            <div class="repas">
                                <div>
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/iconeRondMenu.svg" alt="rond">
                                    <p class="descriptionRepasEmporter"><?php the_field('nom_du_repas'); ?></p>
                                </div>
                                <p class="prixRepas"><?php the_field('prix_du_repas'); ?></p>
                            </div>

                        <?php endif; ?>

                    <?php endwhile; ?>

                <?php else: ?>
                <?php endif; ?>

            </div>



            <div class="repasEmporter repasPrincipaux" id="repasPrincipaux">
                <?php query_posts(array('post_type' => 'post_type_repas_empo')); ?>

                <?php if ( have_posts()) : ?>

                    <?php while (have_posts()) : the_post(); ?>

                        <?php if ( get_field('type_de_repas') == 'Principaux') : ?>

                            <div class="repas">
                                <div>
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/iconeRondMenu.svg" alt="rond">
                                    <p class="descriptionRepasEmporter"><?php the_field('nom_du_repas'); ?></p>
                                </div>
                                <p class="prixRepas"><?php the_field('prix_du_repas'); ?></p>
                            </div>

                        <?php endif; ?>

                    <?php endwhile; ?>

                <?php else: ?>
                <?php endif; ?>

            </div>

            <div class="repasEmporter repasACotes" id="repasACotes">
                <?php query_posts(array('post_type' => 'post_type_repas_empo')); ?>

                <?php if ( have_posts()) : ?>

                    <?php while (have_posts()) : the_post(); ?>

                        <?php if ( get_field('type_de_repas') == 'À côté') : ?>

                            <div class="repas">
                                <div>
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/iconeRondMenu.svg" alt="rond">
                                    <p class="descriptionRepasEmporter"><?php the_field('nom_du_repas'); ?></p>
                                </div>
                                <p class="prixRepas"><?php the_field('prix_du_repas'); ?></p>
                            </div>

                        <?php endif; ?>

                    <?php endwhile; ?>

                <?php else: ?>
                <?php endif; ?>
            </div>

            <div class="repasEmporter repasDessert" id="repasDessert">
                <?php query_posts(array('post_type' => 'post_type_repas_empo')); ?>

                <?php if ( have_posts()) : ?>

                    <?php while (have_posts()) : the_post(); ?>

                        <?php if ( get_field('type_de_repas') == 'Dessert') : ?>

                            <div class="repas">
                                <div>
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/iconeRondMenu.svg" alt="rond">
                                    <p class="descriptionRepasEmporter"><?php the_field('nom_du_repas'); ?></p>
                                </div>
                                <p class="prixRepas"><?php the_field('prix_du_repas'); ?></p>
                            </div>

                        <?php endif; ?>

                    <?php endwhile; ?>

                <?php else: ?>
                <?php endif; ?>
            </div>



        </section>

    </section>



<?php get_footer(); ?>