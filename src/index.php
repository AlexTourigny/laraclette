<?php get_header(); ?>

    <!--------------------------------------------------------------->
    <!-- SECTION HERO -->
    <section class="section-hero">
        <article>
            <h1>La Raclette</h1>
            <h2>Cuisine Suisse et Européenne</h2>
            <hr class="ligne">
            <h3>APPORTEZ VOTRE VIN</h3>
            <a href="" class="hvr-sweep-to-right">Réserver</a>

            <?php wp_nav_menu(array(
                'theme_location' => 'menu_sociaux'
            )); ?>

        </article>
        <aside>
            <div class="img1"></div>
            <div class="img2"></div>
            <div class="trip-advisor">
                <div>
                    <p>Certificats d'Excellence</p>
                    <p>2013 --- 2015</p>
                    <p>2016 --- 2017</p>
                </div>
                <a href=""><img src="images/trip_advisor.png" alt="TripAdvisor"></a>
            </div>
        </aside>
    </section>
    <!--------------------------------------------------------------->


    <!--------------------------------------------------------------->
    <!-- SECTION VIDÉO -->
    <section class="section-video">
        <div>
            <div></div>
            <h1>Nous voir en action</h1>
            <div></div>
        </div>

        <iframe width="795" height="447" src="https://www.youtube.com/embed/FyUMNlenDH0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </section>
    <!--------------------------------------------------------------->


    <!--------------------------------------------------------------->
    <!-- SECTION BIENVENUE -->
    <section class="section-bienvenue">
        <article>
            <div>
                <p>«</p>
                <p>Au nom de toute notre équipe, nous remercions notre fidèle clientèle qui nous fait confiance année après année. </p>
                <p>»</p>
            </div>
        </article>
        <aside>
            <h3>Bienvenue au Restaurant La Raclette!</h3>
            <p>Chez nous, depuis <span>plus de 30 ans</span>, le fromage est <span>fondant</span>, les plats sont <span>savoureux</span>, l’ambiance <span>conviviale</span> vous réchauffent le cœur et votre vin est le bienvenu. En cuisine, le chef Selven Nellatamby compte <span>plus de 20 années d’expérience</span> en cuisine internationale et en pâtisserie notament pour l’Hôtel 5 étoiles Le Paradis de l’île Maurice. En salle à manger, notre équipe de professionnels s’applique à vous faire vivre <span>une expérience unique à Montréal</span>.</p>
        </aside>
    </section>
    <!--------------------------------------------------------------->


    <!--------------------------------------------------------------->
    <!-- SECTION OFFRE -->
    <section class="section-offre">
        <h3>Nous vous offrons...</h3>
        <article>
            <div>
                <p class="further"></p>
                <p class="around"></p>
                <p class="shown">des repas 2 services</p>
                <p class="around">des repas 3 services</p>
                <p class="further">un menu varié</p>
                <p class="fading">des plats à partager</p>
                <p class="hidden">de la cuisson sur pierrades</p>
                <p class="hidden">des desserts maison</p>
            </div>
        </article>
        <aside>
            <a href="">VOIR LE MENU</a>
        </aside>
    </section>
    <!--------------------------------------------------------------->


    <!--------------------------------------------------------------->
    <!-- SECTION TRAITEUR -->
    <section class="sectionTraiteur">

        <div class="divImgTraiteur">

            <img src="images/traiteur1.png" alt="Champagne"/>
            <img src="images/traiteur2.png" class="imageGauche" alt="Entrée de crevettes"/>
            <img src="images/traiteur3.png" alt="Assiette dessert"/>

        </div>

        <div class="divContentTraiteur">
            <h3>Notre traiteur à votre service</h3>

            <div class="blocBlancTraiteur">
                <h4>Service de traiteur évènementiel à domicile, bureau ou en salle de réception</h4>
                <p>Pour un <span>évènement d’entreprise</span>, un <span>cocktail dinatoire</span>, un <span>mariage</span>,
                    un <span>lancement de produit</span> ou un <span>anniversaire</span>, notre service de raclette,
                    fondue au fromage ou pierrade saura <span>régaler vos convives</span>!</p>
            </div>

            <a href="contact.html">EN SAVOIR PLUS</a>

        </div>

    </section>
    <!--------------------------------------------------------------->


    <!--------------------------------------------------------------->
    <!-- SECTION QUOI DE NEUF -->
    <section class="quoiNeuf">
        <h3>Quoi de neuf ?</h3>

        <div class="divQuoiNeuf">

            <div>
                <p>Découvrez notre tout nouveau service de cuisson sur pierre chaudes !</p>
            </div>

            <div>
                <p>Découvrez la célèbre « Zuger Kirschtorte ». Un Gâteau au Kirsch qui vous réchauffera le coeur et l’âme
                    !</p>
            </div>

        </div>

    </section>
    <!--------------------------------------------------------------->


    <!--------------------------------------------------------------->
    <!-- SECTION CONTACT -->
    <section class="contactA">
        <article>

            <!--        <iframe src="https://snazzymaps.com/embed/196287" width="100%" height="600px" style="border:none;"></iframe>-->
        </article>


        <aside>
            <h3>Où nous trouver</h3>
            <i class="fas fa-map-marker-alt"></i>
            <p>
                1059, Gilford (angle Christophe-Colomb)
                <br>Montréal, Québec, H2J 1P7<br>Metro Laurier, sortie: St. Joseph
            </p>
            <i class="fas fa-phone"></i>
            <p>(514) 524-8118</p>

            <h4>Heures d’ouverture</h4>
            <div class="ouverture">
                <p>Mercredi et Dimanche<br>17h30 - 21h00</p>
                <p>Jeudi, Vendredi, Samedi<br>17h30 - 22h00</p>
            </div>
        </aside>
    </section>
    <!--------------------------------------------------------------->

<?php get_footer(); ?>