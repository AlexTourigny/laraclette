<?php get_header(); ?>


<div class="wrapper404">

    <div class="erreurContenu">
        <h2>404</h2>

        <div class="erreurDessous">
            <h3>Non d’un <strong>FROMAGE</strong>!</h3>
            <p class="erreurText">Retourner rapidement à l’accueil avant de<br>vous perdre dans le fromage.</p>
            <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><p>Accueil</p></a>
        </div>
    </div>


    <div class="erreurImage">
        <img src="<?php echo get_template_directory_uri(); ?>/images/fromage.svg" alt="fromage404">
    </div>
    

</div>



<?php get_footer(); ?>