<!--------------------------------------------------------------->
<!-- FOOTER -->
<footer>
    <div class="pied-de-page">
        <a href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo('template_url'); ?>/images/logo.png" alt="Logo pied de page"></a>

        <?php wp_nav_menu(array(
            'theme_location' => 'menu_footer'
        )); ?>

        <?php wp_nav_menu(array(
            'theme_location' => 'menu_sociaux_footer'
        )); ?>

    </div>
    <div class="infolettre">
        <h4>Restez au courant </h4>
        <p>Inscrivez-vous à l’infolettre dès maintenant pour ne rien manquer.</p>
        <form action="index.php" method="post" name="form-infolettre" class="form-infolettre" enctype="multipart/form-data">
            <input id="contact-nom" name="contact-nom" type="email" placeholder="Votre adresse courriel" required>
            <input id="submit" type="submit" name="submit" value="S'INSCRIRE">
        </form>
        <hr class="ligne-footer">
        <small>Tous droits réservés © La Raclette</small>
    </div>
</footer>
<!--------------------------------------------------------------->


<?php wp_footer(); ?>
</body>
</html>