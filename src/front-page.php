<!-- INCLUSION DU HEADER -->
<?php get_header(); ?>

<?php if (have_posts()) : ?>
    <?php while (have_posts()) : the_post(); ?>

        <!--------------------------------------------------------------->
        <!-- SECTION HERO -->
        <section class="section-hero">
            <article>
                <h1><?php bloginfo('title'); ?></h1>
                <h2><?php bloginfo('description'); ?></h2>
                <hr class="ligne">
                <h3>APPORTEZ VOTRE VIN</h3>

                <?php if (get_field('acf_accueil_lien_reservation')) : ?>
                    <a href="<?php the_field('acf_accueil_lien_reservation'); ?>"
                       class="hvr-sweep-to-right">Réserver</a>
                <?php else : ?>
                    <a href="#" class="hvr-sweep-to-right">Réserver</a>
                <?php endif; ?>

                <?php wp_nav_menu(array(
                    'theme_location' => 'menu_sociaux'
                )); ?>

            </article>
            <aside>
                <div class="img1"></div>
                <div class="img2"></div>
                <div class="trip-advisor">
                    <div>
                        <p>Certificats d'Excellence</p>
                        <p>2013 --- 2015</p>
                        <p>2016 --- 2017</p>
                    </div>
                    <a href=""><img src="<?php bloginfo('template_url'); ?>/images/trip_advisor.png" alt="TripAdvisor"></a>
                </div>
            </aside>
        </section>
        <!--------------------------------------------------------------->


        <!--------------------------------------------------------------->
        <!-- SECTION VIDÉO -->
        <section class="section-video">
            <div>
                <div></div>
                <h1>Nous voir en action</h1>
                <div></div>
            </div>

            <?php if (get_field('acf_accueil_video')) : ?>
                <?php the_field('acf_accueil_video'); ?>
            <?php else : ?>
                <p>Il n'y a pas de vidéo présentement.</p>
            <?php endif; ?>
        </section>
        <!--------------------------------------------------------------->


        <!--------------------------------------------------------------->
        <!-- SECTION BIENVENUE -->
        <section class="section-bienvenue">
            <article>
                <?php if (get_field('acf_accueil_citation')) : ?>

                    <div>
                        <p>«</p>
                        <p><?php the_field('acf_accueil_citation'); ?></p>
                        <p>»</p>
                    </div>
                <?php else : ?>
                    <!-- La citation n'apparaît juste pas -->
                <?php endif; ?>
            </article>
            <aside>
                <?php the_content(); ?>
            </aside>
        </section>
        <!--------------------------------------------------------------->


        <!--------------------------------------------------------------->
        <!-- SECTION OFFRE -->
        <section class="section-offre">
            <h3>Nous vous offrons...</h3>
            <article>
                <div>
                    <p class="further"></p>
                    <p class="around"></p>
                    <p class="shown">des repas 2 services</p>
                    <p class="around">des repas 3 services</p>
                    <p class="further">un menu varié</p>
                    <p class="fading">des plats à partager</p>
                    <p class="hidden">de la cuisson sur pierrades</p>
                    <p class="hidden">des desserts maison</p>
                </div>
            </article>
            <aside>
                <?php if (get_field('acf_accueil_lien_menu')) : ?>
                    <a href="<?php the_field('acf_accueil_lien_menu'); ?>">VOIR LE MENU</a>
                <?php else : ?>
                    <!-- Pas de bouton -->
                <?php endif; ?>

            </aside>
        </section>
        <!--------------------------------------------------------------->


        <!--------------------------------------------------------------->
        <!-- SECTION TRAITEUR -->
        <section class="sectionTraiteur">

            <div class="divImgTraiteur">

                <img src="<?php bloginfo('template_url'); ?>/images/traiteur1.png" alt="Champagne"/>
                <img src="<?php bloginfo('template_url'); ?>/images/traiteur2.png" class="imageGauche"
                     alt="Entrée de crevettes"/>
                <img src="<?php bloginfo('template_url'); ?>/images/traiteur3.png" alt="Assiette dessert"/>

            </div>

            <div class="divContentTraiteur">
                <h3>Notre traiteur à votre service</h3>

                <div class="blocBlancTraiteur">
                    <h4>Service de traiteur évènementiel à domicile, bureau ou en salle de réception</h4>
                    <p>Pour un <span>évènement d’entreprise</span>, un <span>cocktail dinatoire</span>, un
                        <span>mariage</span>,
                        un <span>lancement de produit</span> ou un <span>anniversaire</span>, notre service de raclette,
                        fondue au fromage ou pierrade saura <span>régaler vos convives</span>!</p>
                </div>

                <?php if (get_field('acf_accueil_lien_traiteur')) : ?>
                    <a href="<?php the_field('acf_accueil_lien_traiteur'); ?>">EN SAVOIR PLUS</a>
                <?php else : ?>
                    <!-- Pas de bouton -->
                <?php endif; ?>

            </div>

        </section>
        <!--------------------------------------------------------------->


        <!--------------------------------------------------------------->
        <!-- SECTION QUOI DE NEUF -->
        <section class="quoiNeuf">
            <h3>Quoi de neuf ?</h3>

            <div class="divQuoiNeuf">

                <?php if (have_rows('acf_accueil_neuf')): ?>
                    <?php while (have_rows('acf_accueil_neuf')):
                        the_row(); ?>

                        <?php if (get_sub_field('acf_accueil_neuf_image')) : ?>
                        <?php $img = get_sub_field('acf_accueil_neuf_image'); ?>

                        <div style=" background: url('<? echo $img['url'] ?>') center center/cover no-repeat">
                            <?php if (get_sub_field('acf_accueil_neuf_texte')) : ?>
                                <p><?php the_sub_field('acf_accueil_neuf_texte'); ?></p>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>

                    <?php endwhile; ?>
                <?php endif; ?>

            </div>

        </section>
        <!--------------------------------------------------------------->


        <!--------------------------------------------------------------->
        <!-- SECTION CONTACT -->
        <section class="contactA">
            <article>

                <iframe src="https://snazzymaps.com/embed/196287" width="100%" height="600px"
                        style="border:none;"></iframe>
            </article>


            <aside>
                <h3>Où nous trouver</h3>
                <i class="fas fa-map-marker-alt"></i>
                <p style="width: 55%">
                    <?php the_field('acf_og_adresse', 'option'); ?>
                </p>
                <i class="fas fa-phone"></i>
                <p><?php the_field('acf_of_telephone', 'option'); ?></p>

                <h4>Heures d’ouverture</h4>
                <div class="ouverture">
                    <?php if (have_rows('acf_og_heures', 'option')): ?>
                        <?php while (have_rows('acf_og_heures', 'option')):
                            the_row(); ?>

                            <p><?php the_sub_field('acg_of_heure_jours'); ?><br><?php the_sub_field('acg_og_heures_specifique'); ?></p>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </div>
            </aside>
        </section>
        <!--------------------------------------------------------------->

    <?php endwhile; ?>
<?php else : ?>

<?php endif; ?>

<?php get_footer(); ?>