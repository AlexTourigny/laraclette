<?php
/* Template Name: Page contact */

get_header();
?>

<?php if( have_posts() ) : ?>
    <?php while (have_posts()) : the_post(); ?>

    <div class="contactContainer">

        <h3><?php the_title(); ?></h3>
        <hr>

        <section class="sectionContact">

            <div class="contenuSectionContact">

                <div class="adresseSectionContact">
                    <i class="fas fa-map-marker-alt"></i>
                    <p>1059, Gilford (angle Christophe-Colomb)<br>
                        Montréal, Québec, H2J 1P7<br>
                        Metro Laurier, sortie: St. Joseph</p>
                </div>

                <div class="telephoneSectionContact">
                    <i class="fas fa-phone"></i>
                    <p><?php  the_field('acf_of_telephone', 'option'); ?></p>
                </div>

                <section class="ouvertureSectionContact">
                    <h4>Heures d’ouverture</h4>

                    <div class="heuresOuvertureContact heuresPremier">
                        <div>
                            <p>Mercredi et Dimanche</p>
                            <p>17h30 - 21h00</p>
                        </div>

                        <div>
                            <p>Jeudi, Vendredi, Samedi</p>
                            <p>17h30 - 22h00</p>
                        </div>
                    </div>

                    <p><?php  the_field('acf_og_courriel', 'option'); ?></p>

                </section>

            </div>

            <div class="mapSectionContact">
                <iframe src="https://snazzymaps.com/embed/196287" width="100%" height="600px" style="border:none;"></iframe>
            </div>

        </section>

        <div class="wrapper">
            <?php the_content(); ?>
        </div>

    </div>

    <?php endwhile; ?>
<?php endif; ?>


<?php get_footer(); ?>