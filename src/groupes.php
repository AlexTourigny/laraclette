<?php
/* Template Name: groupes */
?>

<?php get_header(); ?>
    <!--------------------------------------------------------------->

<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>

    <div class="fond" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/fond-mix.jpg.');">
        <div class="wrapper">
            <h3><?php the_title(); ?></h3>
        </div>
        <div class="swiper-container wrapper">
            <div class="swiper-wrapper">
<!---->
                <div class="swiper-slide"><img src="<?php echo get_template_directory_uri(); ?>/images/swiper1.jpg" height="413" width="1070"/> </div>
                <div class="swiper-slide"><img src="<?php echo get_template_directory_uri(); ?>/images/swiper2.jpg" height="413" width="1070"/></div>
                <div class="swiper-slide"><img src="<?php echo get_template_directory_uri(); ?>/images/swiper3.png" height="413" width="1070"/></div>

            </div>
            <div class="swiper-pagination"></div>
        </div>

    </div>
    <div  class="wrapper groupes">
        <?php the_content(); ?>
    </div>


    <?php endwhile; ?>
<?php else : ?>

    <!– S’il n'y a pas de post, j'affiche cette partie -->

<?php endif; ?>

<?php get_footer(); ?>
