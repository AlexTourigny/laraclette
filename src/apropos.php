<?php
/* Template Name: APropos */
?>

<?php get_header(); ?>
    <!--------------------------------------------------------------->

<?php if (have_posts()) : ?>
    <?php while (have_posts()) : the_post(); ?>


    <div class="wrapper a-propos">
        <h3><?php the_title(); ?></h3>

        <div class="section-apropos">
            <div>
                <div class="texture-rouge" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/texturebg_rouge.jpg');"></div>
               <?php echo the_post_thumbnail('big'); ?>
            </div>
            <div style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/texturebg_creme.jpg')">
                <?php the_content(); ?>

            </div>
        </div>
    </div>




    <?php endwhile; ?>
<?php else : ?>

    <!– S’il n'y a pas de post, j'affiche cette partie -->

<?php endif; ?>

<?php get_footer(); ?>

