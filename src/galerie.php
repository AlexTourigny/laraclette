<?php
/* Template Name: galerie */
?>

<?php get_header(); ?>
    <!--------------------------------------------------------------->
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
     <div class="galerie wrapper">


         <h3><?php the_title(); ?></h3>

         <div class="texture-rouge" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/texturebg_rouge.jpg');"></div>
         <div class="texture-creme"style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/texturebg_creme.jpg');"></div>

         <div class="container">
             <img  src="<?php echo get_template_directory_uri(); ?>/images/hero_img1.jpg" id="expandedImg">

         </div>

        <div class="row">
            <div class="column">
                <img src="<?php echo get_template_directory_uri(); ?>/images/imgGalerie/g1.jpg" alt="La Raclette" style="width:100%;filter: grayscale(100%);" onclick="myFunction(this);">
            </div>
            <div class="column">
                <img src="<?php echo get_template_directory_uri(); ?>/images/imgGalerie/g2.jpg" alt="La Raclette" style="width:100%;filter: grayscale(100%);" onclick="myFunction(this);">
            </div>
            <div class="column">
                <img src="<?php echo get_template_directory_uri(); ?>/images/imgGalerie/g3.jpg" alt="La Raclette" style="width:100%;filter: grayscale(100%);" onclick="myFunction(this);">
            </div>
            <div class="column">
                <img src="<?php echo get_template_directory_uri(); ?>/images/imgGalerie/g4.jpg" alt="La Raclette" style="width:100%;filter: grayscale(100%);" onclick="myFunction(this);">
            </div>
            <div class="column">
                <img src="<?php echo get_template_directory_uri(); ?>/images/imgGalerie/g8.jpg" alt="La Raclette" style="width:100%;filter: grayscale(100%);" onclick="myFunction(this);">
            </div>
            <div class="column">
                <img src="<?php echo get_template_directory_uri(); ?>/images/imgGalerie/g9.jpg" alt="La Raclette" style="width:100%;filter: grayscale(100%);" onclick="myFunction(this);">
            </div>
            <div class="column">
                <img src="<?php echo get_template_directory_uri(); ?>/images/imgGalerie/g11.jpg" alt="La Raclette" style="width:100%;filter: grayscale(100%);" onclick="myFunction(this);">
            </div>                                                   
            <div class="column">                                     
                <img src="<?php echo get_template_directory_uri(); ?>/images/hero_img2.jpg" alt="La Raclette" style="width:100%;filter: grayscale(100%);" onclick="myFunction(this);">
            </div>
        </div>


    </div>


    <!--------------------------------------------------------------->

    <?php endwhile; ?>
<?php else : ?>

    <!– S’il n'y a pas de post, j'affiche cette partie -->

<?php endif; ?>
<script>
    function myFunction(imgs) {
        var expandImg = document.getElementById("expandedImg");
        var imgText = document.getElementById("imgtext");
        expandImg.src = imgs.src;
        imgText.innerHTML = imgs.alt;
        expandImg.parentElement.style.display = "flex";
    }
</script>
<?php get_footer(); ?>
