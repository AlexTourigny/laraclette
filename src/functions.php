<?php

// FONCTIONS NÉCESSAIRES AU THÈME

// Autorisation des images à la une
add_theme_support('post-thumbnails');
set_post_thumbnail_size(1280, 720);


// Création des menus
function enregistrer_menu()
{
    register_nav_menus(array(
        'menu_principal' => 'Menu principal',
        'menu_langues' => 'Menu des langes',
        'menu_footer' => 'Menu du pied de page',
        'menu_sociaux' => 'Menu des réseaux sociaux',
        'menu_sociaux_footer' => 'Menu des réseaux sociaux pied de page',
    ));
}
add_action('init', 'enregistrer_menu');


// Actionation des options générales
if( function_exists('acf_add_options_page') ) {
    acf_add_options_page(array(
        'page_title' => 'Options générales de mon thème',
        'menu_title' => 'Options générales',
        'menu_slug' => 'la-raclette-options-generales',
        'capability' => 'edit_posts',
        'redirect' => false
    ));
}


// Register Custom Post Type
function custom_post_type_emporter() {

    $labels = array(
        'name'                  => _x( 'Repas à emporter', 'Post Type General Name', 'repas_emporter' ),
        'singular_name'         => _x( 'Repas à emporter', 'Post Type Singular Name', 'repas_emporter' ),
        'menu_name'             => __( 'Repas à emporter', 'repas_emporter' ),
        'name_admin_bar'        => __( 'Post Type', 'repas_emporter' ),
        'archives'              => __( 'Item Archives', 'repas_emporter' ),
        'attributes'            => __( 'Item Attributes', 'repas_emporter' ),
        'parent_item_colon'     => __( 'Parent Item:', 'repas_emporter' ),
        'all_items'             => __( 'Tous les repas', 'repas_emporter' ),
        'add_new_item'          => __( 'Ajouter nouveau repas', 'repas_emporter' ),
        'add_new'               => __( 'Ajouter repas', 'repas_emporter' ),
        'new_item'              => __( 'New Item', 'repas_emporter' ),
        'edit_item'             => __( 'Edit Item', 'repas_emporter' ),
        'update_item'           => __( 'Update Item', 'repas_emporter' ),
        'view_item'             => __( 'View Item', 'repas_emporter' ),
        'view_items'            => __( 'View Items', 'repas_emporter' ),
        'search_items'          => __( 'Search Item', 'repas_emporter' ),
        'not_found'             => __( 'Aucun repas trouvé', 'repas_emporter' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'repas_emporter' ),
        'featured_image'        => __( 'Featured Image', 'repas_emporter' ),
        'set_featured_image'    => __( 'Set featured image', 'repas_emporter' ),
        'remove_featured_image' => __( 'Remove featured image', 'repas_emporter' ),
        'use_featured_image'    => __( 'Use as featured image', 'repas_emporter' ),
        'insert_into_item'      => __( 'Insert into item', 'repas_emporter' ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', 'repas_emporter' ),
        'items_list'            => __( 'Items list', 'repas_emporter' ),
        'items_list_navigation' => __( 'Items list navigation', 'repas_emporter' ),
        'filter_items_list'     => __( 'Filter items list', 'repas_emporter' ),
    );
    $args = array(
        'label'                 => __( 'Repas à emporter', 'repas_emporter' ),
        'description'           => __( 'Repas à emporter', 'repas_emporter' ),
        'labels'                => $labels,
        'supports'              => array( 'title' ),
        'taxonomies'            => array( 'category', 'post_tag' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
    );
    register_post_type( 'post_type_repas_empo', $args );

}
add_action( 'init', 'custom_post_type_emporter', 0 );



