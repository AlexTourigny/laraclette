document.addEventListener("DOMContentLoaded", function(event) {

    document.getElementById('choixEntrees').addEventListener("click", function () {
        document.getElementById("repasEntrees").style.display = "flex";

        document.getElementById("repasDessert").style.display = "none";
        document.getElementById("repasPrincipaux").style.display = "none";
        document.getElementById('repasACotes').style.display = "none";
    });

    document.getElementById('choixPlatsPrincipaux').addEventListener("click", function () {
        document.getElementById("repasPrincipaux").style.display = "flex";

        document.getElementById("repasEntrees").style.display = "none";
        document.getElementById("repasDessert").style.display = "none";
        document.getElementById('repasACotes').style.display = "none";
    });

    document.getElementById('choixACotes').addEventListener("click", function () {
        document.getElementById('repasACotes').style.display = "flex";

        document.getElementById("repasEntrees").style.display = "none";
        document.getElementById("repasPrincipaux").style.display = "none";
        document.getElementById('repasDessert').style.display = "none";
    });

    document.getElementById('choixDesserts').addEventListener("click", function () {
        document.getElementById('repasDessert').style.display = "flex";

        document.getElementById("repasEntrees").style.display = "none";
        document.getElementById("repasPrincipaux").style.display = "none";
        document.getElementById('repasACotes').style.display = "none";
    });


});