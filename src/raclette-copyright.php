<?php
/* Template Name: Page copyright */

get_header();
?>

<?php if( have_posts() ) : ?>
    <?php while (have_posts()) : the_post(); ?>

    <div class="contactContainer">

        <h3><?php the_title(); ?></h3>
        <hr>

        <div class="container">
            <?php the_content(); ?>
        </div>


    </div>

    <?php endwhile; ?>
<?php endif; ?>


<?php get_footer(); ?>